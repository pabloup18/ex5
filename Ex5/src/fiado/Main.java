package fiado;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite o nome do consumidor: ");
		Consumidor consumidor = new Consumidor(input.nextLine());
		
		System.out.print("Digite o telefone de " + consumidor.getNome() + ":");
		consumidor.setTelefone(input.nextLine());
		
		System.out.print("Informe o valor da compra: ");
		int fiado = input.nextInt();
		consumidor.setFiado(fiado);
		
		int total = consumidor.getFiado();
		if (total > 100) {
			System.out.println("Cliente: "+ consumidor.getNome() + " com telefone: "+ consumidor.getTelefone() + " deve: R$" + consumidor.getFiado() + " voce esta devendo + de 100");
		}else {
			System.out.println("Cliente: "+ consumidor.getNome() + " com telefone: "+ consumidor.getTelefone() + " deve: R$" + consumidor.getFiado());
		}
	}
}
